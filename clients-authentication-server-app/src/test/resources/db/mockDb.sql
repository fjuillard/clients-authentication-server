

-- mock table  t_client
CREATE TABLE t_client
(
  id bigint NOT NULL,
  date_naissance date NOT NULL,
  password character varying(255) NOT NULL,
  email character varying(255) NOT NULL
);

INSERT INTO t_client (id, date_naissance, email, password) VALUES (1, '1920-01-01', 'aaa@bbb.com', '5285eacd846322dbf9e22c92cc03150921ed4278')
