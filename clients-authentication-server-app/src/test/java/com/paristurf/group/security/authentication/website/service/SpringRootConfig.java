package com.paristurf.group.security.authentication.website.service;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.paristurf.group.security.authentication.dao.ClientDao;
import com.paristurf.group.security.authentication.service.LdapUserAuthenticationService;
import com.paristurf.group.security.authentication.website.BettingClientAuthenticationService;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
public class SpringRootConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    Environment env;

    @Bean
    public BettingClientAuthenticationService defaultBettingClientAuthenticationService() {
        return new DefaultBettingClientAuthenticationService("key");
    }

    // Mock JdbcTemplate by giving it a embedded db
    @Bean(name = "hrJdbcTemplate")
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean(name = "hrNamedJdbcTemplate")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }

    @Bean
    public ClientDao clientDao() {
        return new ClientDao();
    }

    @Bean
    public DataSource dataSource() {
        // no need to shutdown, EmbeddedDatabaseFactoryBean will take care of
        // this
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2) // .H2
            .addScript("db/mockDb.sql").build();
        return db;
    }
}