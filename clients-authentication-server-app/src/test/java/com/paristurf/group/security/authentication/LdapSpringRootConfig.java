package com.paristurf.group.security.authentication;

import com.paristurf.group.security.authentication.service.LdapUserAuthenticationService;
import com.paristurf.group.security.authentication.service.UserContextMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
public class LdapSpringRootConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    Environment env;

    @Bean
    public LdapUserAuthenticationService defaultClientAuthenticationService() {
        return new LdapUserAuthenticationService("ddd");
    }

    @Bean(name = "authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication().userDnPatterns("uid={0},ou=Users").userSearchBase("dc=genyinfos,dc=com")
            .groupSearchBase("ou=Groups").groupRoleAttribute("cn").groupSearchFilter("member={0}")
            .userDetailsContextMapper(new UserContextMapper())
            // .ldapAuthoritiesPopulator(ldapAuthoritiesPopulator())
            .contextSource().ldif("classpath:ldap/ldap.ldif").root("dc=genyinfos,dc=com")
            .managerDn("uid=binder,ou=Users,dc=genyinfos,dc=com").managerPassword("LD4pB!nd3r!?");
    }

    @Bean
    public LdapAuthoritiesPopulator ldapAuthoritiesPopulator() throws Exception {
        DefaultLdapAuthoritiesPopulator populator = new DefaultLdapAuthoritiesPopulator(contextSource(), "ou=Groups");
        populator.setIgnorePartialResultException(true);
        populator.setSearchSubtree(true);
        return populator;
    }

    @Bean
    public BaseLdapPathContextSource contextSource() throws Exception {
        DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource("ldap://kyros:389/");
        contextSource.setUserDn("uid=binder,ou=Users,dc=genyinfos,dc=com");
        contextSource.setPassword("LD4pB!nd3r!?");
        contextSource.setBase("dc=genyinfos,dc=com");
        contextSource.afterPropertiesSet();
        return contextSource;
    }

}