package com.paristurf.group.security.authentication.website.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.paristurf.group.security.authentication.domainmodel.LoginUserBo;
import com.paristurf.group.security.authentication.domainmodel.website.BettingCredentials;
import com.paristurf.group.security.authentication.website.BettingClientAuthenticationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringRootConfig.class)
@Configuration
public class DefaultClientAuthenticationServiceTest {

    @Autowired
    private BettingClientAuthenticationService service;

    private BettingCredentials login;

    private LoginUserBo loginUserBo;

    @Before
    public void init() {
        login = new BettingCredentials();
        login.setBirthday("01/01/1920");
        login.setEmail("aaa@bbb.com");
        login.setPassword("123456789");

        loginUserBo = new LoginUserBo();
        loginUserBo.setPassword("3601Geny");
        loginUserBo.setUsername("testuser");
    }

    @Test
    public void testAuthenticate_defaultCase() {
        Assert.assertNotNull(service.authenticateUser(login));
    }

    @Test
    public void testAuthenticate_nullField() {
        login.setBirthday(null);
        Assert.assertNull(service.authenticateUser(login));

        init();
        login.setEmail(null);
        Assert.assertNull(service.authenticateUser(login));

        init();
        login.setPassword(null);
        Assert.assertNull(service.authenticateUser(login));
    }

    @Test
    public void testAuthenticate_wrongInformations() {
        login.setBirthday("01/01/1921");
        Assert.assertNull(service.authenticateUser(login));

        init();
        login.setEmail("aaa@bbb.cpm");
        Assert.assertNull(service.authenticateUser(login));

        init();
        login.setPassword("12345678");
        Assert.assertNull(service.authenticateUser(login));
    }
}
