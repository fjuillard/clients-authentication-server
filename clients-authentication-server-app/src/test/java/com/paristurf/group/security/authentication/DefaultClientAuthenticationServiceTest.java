package com.paristurf.group.security.authentication;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.paristurf.group.security.authentication.domainmodel.LoginUserBo;
import com.paristurf.group.security.authentication.domainmodel.exception.ClientNotFoundException;
import com.paristurf.group.security.authentication.service.LdapUserAuthenticationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = LdapSpringRootConfig.class)
@Ignore
//TODO fix tests
public class DefaultClientAuthenticationServiceTest {

    @Autowired
    private LdapUserAuthenticationService authService;

    private LoginUserBo loginUserBo;

    @Before
    public void init() {
        loginUserBo = new LoginUserBo();
        loginUserBo.setPassword("3601Geny");
        loginUserBo.setUsername("testuser");
    }

    @Test
    public void testLoginWithLdap_defaultCase() throws ClientNotFoundException {
        Assert.assertNotNull(authService.loginWithLdap(loginUserBo));
    }

    @Test(expected = ClientNotFoundException.class)
    public void testLoginWithLdap_badCredentials() throws ClientNotFoundException {
        loginUserBo.setUsername("jchirac");
        authService.loginWithLdap(loginUserBo);
    }

}
