package com.paristurf.group.security.authentication.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.paristurf.group.security.authentication.service.LdapUserAuthenticationService;
import com.paristurf.group.security.authentication.service.config.SecurityConfig;
import com.paristurf.group.security.authentication.website.BettingClientAuthenticationService;
import com.paristurf.group.security.authentication.website.service.DefaultBettingClientAuthenticationService;

@Configuration
@ComponentScan("com.paristurf.group.security.authentication")
@Import(value = { PersistenceConfig.class, SecurityConfig.class })
public class ApplicationConfiguration {

    @Value("${architecture.servers.token.signingKey}")
    private String key;

    @Bean
    public LdapUserAuthenticationService ldapUserAuthenticationService() {
        return new LdapUserAuthenticationService(key);
    }

    @Bean("defaultBettingClientAuthenticationService")
    public BettingClientAuthenticationService bettingClientAuthenticationService() {
        return new DefaultBettingClientAuthenticationService(key);
    }

}
