package com.paristurf.group.security.authentication;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.paristurf.group.security.authentication.dao.UserRowMapper;
import com.paristurf.group.security.authentication.domainmodel.User;

@Repository
public class ClientDao {

    /** The jdbc template. */
    @Autowired
    @Qualifier(value = "hrJdbcTemplate")
    protected JdbcTemplate jdbcTemplate;

    /** The named jdbc template. */
    @Autowired
    @Qualifier(value = "hrNamedJdbcTemplate")
    protected NamedParameterJdbcTemplate namedJdbcTemplate;

    public User findUserByLoginInformation(String email, String password, Date birthday) {
        String requestTemplate = "SELECT * FROM t_client WHERE lower(email) = ? AND password = ? AND date_naissance = ?";
        return jdbcTemplate.queryForObject(requestTemplate, new Object[] { email.trim().toLowerCase(), password,
                        birthday }, new UserRowMapper());
    }

    public User findUserByEmail(String email) {
        String requestTemplate = "SELECT * FROM t_client WHERE lower(email) = ?";
        return jdbcTemplate.queryForObject(requestTemplate, new Object[]{ email.trim().toLowerCase() },
                new UserRowMapper());
    }
}
