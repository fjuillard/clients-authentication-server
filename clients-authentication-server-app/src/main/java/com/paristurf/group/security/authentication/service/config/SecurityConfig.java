package com.paristurf.group.security.authentication.service.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

import com.paristurf.group.security.authentication.service.UserContextMapper;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    
    @Value("${architecture.ldap.url}")
    private String ldapUrl;
    
    @Value("${architecture.ldap.dn}")
    private String ldapBinder;
    
    @Value("${architecture.ldap.dnPatterns}")
    private String ldapPatterns;
    
    @Value("${architecture.ldap.password}")
    private String ldapPassword;
    
    @Value("${architecture.ldap.base}")
    private String ldapBase;
    
    @Value("${architecture.ldap.groups}")
    private String ldapGroups;
    
    @Value("${architecture.ldap.groupSearchFilter}")
    private String ldapGroupSearchFilter;
    
    @Bean
    @Qualifier("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .csrf().disable()
        .securityContext().disable()
        .httpBasic().disable()
        .authorizeRequests().anyRequest().permitAll().and()
        .formLogin().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.ldapAuthentication().userDnPatterns(ldapPatterns).userSearchBase(ldapBase)
            .groupRoleAttribute("cn").groupSearchFilter(ldapGroupSearchFilter).userDetailsContextMapper(new UserContextMapper())
            .contextSource(contextSource()).ldapAuthoritiesPopulator(ldapAuthoritiesPopulator());
    }

    @Bean
    public LdapAuthoritiesPopulator ldapAuthoritiesPopulator() throws Exception {
        DefaultLdapAuthoritiesPopulator populator = new DefaultLdapAuthoritiesPopulator(contextSource(), ldapGroups);
        populator.setIgnorePartialResultException(true);
        populator.setSearchSubtree(true);
        return populator;
    }

    @Bean
    public BaseLdapPathContextSource contextSource() throws Exception {
        DefaultSpringSecurityContextSource contextSource = new DefaultSpringSecurityContextSource(ldapUrl);
        contextSource.setUserDn(ldapBinder);
        contextSource.setPassword(ldapPassword);
        contextSource.setBase(ldapBase);
        contextSource.afterPropertiesSet();
        return contextSource;
    }

}
