package com.paristurf.group.security.authentication.service;

import java.util.List;

import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;

/**
 * Mapper servant à faire le passe plat entre l'objet Principal de spring security et l'objet métier User
 * Extends LdapUserDetailsImpl pour éviter de recoder les méthodes de l'interface UserDetails (getAuthorities, etc )
 */
public class UserLdapDetails extends LdapUserDetailsImpl {

    private String username;
    private String fullName;
    private String email;
    private Long id;
    private List<String> roles;

    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString(){
        return super.toString();
    }

}
