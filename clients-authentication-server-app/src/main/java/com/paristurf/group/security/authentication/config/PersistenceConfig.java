package com.paristurf.group.security.authentication.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import com.paristurf.oasis.datasource.DataSourceBuilderFactory;

@Configuration
@EnableTransactionManagement
public class PersistenceConfig implements TransactionManagementConfigurer {

    @Bean(destroyMethod = "")
    public DataSource hrDataSource() {
        return DataSourceBuilderFactory.fromConfiguration("architecture.servers.authentication").build();
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(hrDataSource());
    }

    @Bean
    public NamedParameterJdbcTemplate hrNamedJdbcTemplate() {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(hrDataSource());
        return jdbcTemplate;
    }

    @Bean
    public JdbcTemplate hrJdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(hrDataSource());
        return jdbcTemplate;
    }

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return transactionManager();
    }

}
