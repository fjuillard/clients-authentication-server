package com.paristurf.group.security.authentication.initializer;

import com.paristurf.group.security.authentication.config.ApplicationConfiguration;
import com.paristurf.oasis.web.api.Api;
import com.paristurf.oasis.web.api.ApiBuilder;

@Api(name = "authentication-server", version = "1.0.0")
public class Application {

    public static void main(String[] args) {
        new ApiBuilder(Application.class, ApplicationConfiguration.class)
            .tracing(true).build().run(args);
    }

}
