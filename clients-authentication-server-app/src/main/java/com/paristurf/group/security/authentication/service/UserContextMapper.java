package com.paristurf.group.security.authentication.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

/**
 * Specific mapper for Ldap User.
 * Add email, cn, employeeNumber and uid
 * Transform Authority list into String List
 */
public class UserContextMapper implements UserDetailsContextMapper {

	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {
		UserLdapDetails userDetails = new UserLdapDetails();
		userDetails.setEmail(ctx.getStringAttribute("email"));
        userDetails.setFullName(ctx.getStringAttribute("cn"));
		String employeeNumber = ctx.getStringAttribute("employeenumber");
		if(employeeNumber != null && !employeeNumber.isEmpty()){
			userDetails.setId(Long.valueOf(employeeNumber));
		}
        userDetails.setUsername(ctx.getStringAttribute("uid"));
        List<String> roles = new ArrayList<String>();
        for(GrantedAuthority authority : authorities) {
            roles.add(authority.getAuthority());
        }
        userDetails.setRoles(roles);
		return userDetails;
	}

	@Override
	public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
		
	}
	

}
