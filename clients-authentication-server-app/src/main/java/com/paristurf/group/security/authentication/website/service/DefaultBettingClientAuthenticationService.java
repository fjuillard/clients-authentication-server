package com.paristurf.group.security.authentication.website.service;

import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import com.paristurf.group.security.authentication.dao.ClientDao;
import com.paristurf.group.security.authentication.domainmodel.User;
import com.paristurf.group.security.authentication.domainmodel.website.BettingCredentials;
import com.paristurf.group.security.authentication.website.BettingClientAuthenticationService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class DefaultBettingClientAuthenticationService implements BettingClientAuthenticationService {

    /**
     * Slf4J Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBettingClientAuthenticationService.class);

    @Autowired
    private ClientDao clientDao;

    private Key secret;

    public DefaultBettingClientAuthenticationService(String signingKey) {
        byte[] decodedKey = Base64.decodeBase64(signingKey);
        secret = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }

    @Override
    public String authenticateUser(BettingCredentials credentials) {
        if (credentials.hasNullOrEmptyField()) {
            return null;
        }
        String encodedPassword = new ShaPasswordEncoder().encodePassword(credentials.getPassword(),
            credentials.getEmail());
        // format birthday
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date birthdayDate = null;
        try {
            birthdayDate = formatter.parse(credentials.getBirthday());
            User user = clientDao.findUserByLoginInformation(credentials.getEmail(), encodedPassword, birthdayDate);
            return createTokenForUser(user);
        } catch (ParseException e) {
            LOGGER.error("Unable to parse birthDate : {} ", credentials.getBirthday());
        } catch (EmptyResultDataAccessException e) {
            LOGGER.debug("Could not find user with login info : {}", credentials);
        }
        return null;
    }

    protected String createTokenForUser(User user) {
        if (user != null) {
            Map<String, Object> claims = new HashMap<>();
            claims.put("userId", user.getId());
            return Jwts.builder().setIssuedAt(new Date()).setSubject(user.getEmail())
                .setId(String.valueOf(user.getId())).setExpiration(DateUtils.addMinutes(new Date(), 30))
                .claim("userId", user.getId()).signWith(SignatureAlgorithm.HS512, secret).compact();
        }
        return null;
    }

}
