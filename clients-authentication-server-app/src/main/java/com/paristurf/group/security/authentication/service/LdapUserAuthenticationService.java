package com.paristurf.group.security.authentication.service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.paristurf.group.security.authentication.UserAuthenticationService;
import com.paristurf.group.security.authentication.domainmodel.LoginUserBo;
import com.paristurf.group.security.authentication.domainmodel.User;
import com.paristurf.group.security.authentication.domainmodel.exception.ClientNotFoundException;
import com.paristurf.group.security.authentication.domainmodel.exception.UserNotFoundException;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class LdapUserAuthenticationService implements UserAuthenticationService {

	private static final Logger logger = LoggerFactory.getLogger(LdapUserAuthenticationService.class);

	@Autowired(required = true)
	private AuthenticationManager authenticationManager;

	private Key secret;

	@Autowired
	public LdapUserAuthenticationService(String signingKey) {
		// byte[] decodedKey = Base64.getDecoder().decode(signingKey);
		byte[] decodedKey = org.apache.commons.codec.binary.Base64.decodeBase64(signingKey);
		secret = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
	}

	@Override
	public User loginWithLdap(LoginUserBo login) throws ClientNotFoundException {
		
		logger.info("trying to find user {} in LDAP", login.getUsername());
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());
		User user = null;
		
		try {
			
			logger.info("authToken {}", authToken);
			Authentication a = authenticationManager.authenticate(authToken);
			logger.info("a {}", a);
			UserLdapDetails userDetails = (UserLdapDetails) a.getPrincipal();
			logger.info("userDetails {}", userDetails);
			user = new User(userDetails.getUsername(), userDetails.getFullName(), userDetails.getEmail(), userDetails.getId(), userDetails.getRoles());
			Map<String, Object> claims = new HashMap<>();
			claims.put("userId", user.getId());
			user.setToken(Jwts.builder().setIssuedAt(new Date()).setSubject(user.getEmail()).setExpiration(DateUtils.addMinutes(new Date(), 30)).claim("userId", user.getId())
			    .claim("roles", userDetails.getRoles()).signWith(SignatureAlgorithm.HS512, secret).compact());
			
		} catch (BadCredentialsException e) {
			logger.error("Unable to log with {}", login.getUsername(), e);
			throw new UserNotFoundException("Unable to log user " + login.getUsername() + ". Wrong username or password.");
		}
		return user;
	}
}
