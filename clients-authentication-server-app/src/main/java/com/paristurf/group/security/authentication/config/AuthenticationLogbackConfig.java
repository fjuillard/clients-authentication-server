package com.paristurf.group.security.authentication.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Configuration;

import com.paristurf.oasis.web.api.server.log.AbstractLogbackConfig;

@Configuration
public class AuthenticationLogbackConfig extends AbstractLogbackConfig {

    @Override
    public String getAppName() {
        return "AUTHENTICATION";
    }

    @Override
    public Map<String, String> getRootLevelsPerPackage() {
        return new HashMap<>();
    }
}