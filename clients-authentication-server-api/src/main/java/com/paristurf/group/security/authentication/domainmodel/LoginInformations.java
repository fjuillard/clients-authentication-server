package com.paristurf.group.security.authentication.domainmodel;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LoginInformations {

    private String email;

    private String password;

    /** form dd/MM/yyyy */
    private String birthday;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Boolean hasNullOrEmptyField() {
        if (birthday == null || email == null || password == null || birthday.isEmpty() || email.isEmpty()
            || password.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "LoginInformations [email=" + email + ", password=[ENCRYPTED]" + ", birthday=" + birthday + "]";
    }

    public static void main(String[] args) throws Exception {
        LoginInformations i = new LoginInformations();
        i.setBirthday("01/04/1982");
        i.setEmail("b.dahon@genyinfos.com");
        i.setPassword("Password2");

        System.out.println(new ObjectMapper().writeValueAsString(i));
    }

}
