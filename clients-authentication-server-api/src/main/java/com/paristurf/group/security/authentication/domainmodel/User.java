package com.paristurf.group.security.authentication.domainmodel;

import java.util.List;

public class User {

	protected String username;
	protected String fullName;
	protected String email;
	protected Long id;
	protected List<String> roles;
	protected String token;

	public User() {

	}

	public User(String username, String fullName, String email, Long id, List<String> roles) {
		this.username = username;
		this.fullName = fullName;
		this.email = email;
		this.id = id;
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
