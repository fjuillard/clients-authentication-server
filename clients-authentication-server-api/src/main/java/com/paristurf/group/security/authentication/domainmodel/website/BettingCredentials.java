package com.paristurf.group.security.authentication.domainmodel.website;

public class BettingCredentials {

    private String email;

    private String password;

    /** form dd/MM/yyyy */
    private String birthday;

    public Boolean hasNullOrEmptyField() {
        if (birthday == null || email == null || password == null || birthday.isEmpty() || email.isEmpty()
            || password.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
