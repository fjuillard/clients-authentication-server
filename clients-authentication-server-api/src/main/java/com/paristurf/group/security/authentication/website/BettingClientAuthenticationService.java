package com.paristurf.group.security.authentication.website;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.paristurf.group.security.authentication.domainmodel.website.BettingCredentials;

/**
 * Authentication API for betting websites
 */
@Path("/authentication/betting")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BettingClientAuthenticationService {

    /**
     * User login. Returns a token associated to the provided credentials.
     *
     * @param credentials
     * @return OK / KO
     */
    @POST
    @Path("/genybet")
    String authenticateUser(BettingCredentials credentials);

}
