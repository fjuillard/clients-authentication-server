package com.paristurf.group.security.authentication.domainmodel;

public class Roles {

	public static final String ROLE_OPERATOR = "ROLE_OPERATOR";
	public static final String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";
}
