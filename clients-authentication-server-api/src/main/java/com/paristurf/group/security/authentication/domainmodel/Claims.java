package com.paristurf.group.security.authentication.domainmodel;

public class Claims {
    public static final String USER_ID = "userId";
    public static final String ROLES = "roles";
    public static final String OPERATOR_ID = "operatorId";
    public static final String EMAIL = "email";
    public static final String FULL_NAME = "fullname";
    public static final String ID = "id";
}