package com.paristurf.group.security.authentication;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.paristurf.group.security.authentication.domainmodel.LoginUserBo;
import com.paristurf.group.security.authentication.domainmodel.User;
import com.paristurf.group.security.authentication.domainmodel.exception.ClientNotFoundException;
import com.paristurf.group.security.authentication.domainmodel.exception.UserNotFoundException;

/**
 * Authentication Service for User and BO users.
 * 
 * @author jsanson
 */
@Path("/authentication")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UserAuthenticationService {

    /**
     * BO user login
     *
     * @param login
     * @return User object if login is successful
     * @throws ClientNotFoundException if not
     */
    @POST
    @Path("/bo")
    User loginWithLdap(LoginUserBo login) throws UserNotFoundException;

}
