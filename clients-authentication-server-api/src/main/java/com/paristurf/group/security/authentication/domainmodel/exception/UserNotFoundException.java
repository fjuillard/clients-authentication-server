package com.paristurf.group.security.authentication.domainmodel.exception;

import javax.ws.rs.NotFoundException;

public class UserNotFoundException extends NotFoundException {

	private static final long serialVersionUID = 9005343785410075294L;

	public enum ErrorCodes {
		BAD_CREDENTIALS
	}

	public UserNotFoundException(String message) {
		super(message);
	}

}
