package com.paristurf.group.security.authentication.domainmodel.exception;

import javax.ws.rs.NotFoundException;

public class ClientNotFoundException extends NotFoundException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6939194803522636801L;

    public enum ErrorCodes {
        BAD_CREDENTIALS
    }

    public ClientNotFoundException(String message) {
        super(message);
    }

}
